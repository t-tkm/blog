---
title: "Build a blog system"
date: 2018-04-02T12:55:25+09:00
---
There are many static website generator, Jekyll, Hugo, Hexo etc.. (https://staticsitegenerators.net/)

At first, I attempted to use Jekyll, but since it takes time to configure Ruby system, I abandoned using it. 
Next, tried using Hugo written by Go(https://gohugo.io/), I could build it without problems. So give importance to simplicity, I chose Hugo. 

Struggled a little bit to deploy to github here(using Github Pages), I could complete it with reference to the official documentation(https://gohugo.io/documentation/).

